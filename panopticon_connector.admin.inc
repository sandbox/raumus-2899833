<?php

/**
 * @file
 * Admin forms for Panopticon Connector
 */

/**
 * Create admin page
 */
function panopticon_connector_admin() {
    return drupal_get_form('panopticon_connector_admin_form');
}

/**
 * Implements hook_form()
 */
function panopticon_connector_admin_form() {
    $form = array();
    $form['panopticon_connector_vcode'] = array(
        '#type' => 'textfield',
        '#title' => t('Panopticon Verification Code'),
        '#description' => t('Enter your Panopticon site verification code here.'),
        '#default_value' => variable_get('panopticon_connector_vcode', ''),
        '#weight' => 0,
    );

    $form['update_interval'] = array(
        '#type' => 'textfield',
        '#default_value' => variable_get('update_check_frequency', 1),
        '#title' => 'Update Check Frequency/Interval',
        '#required' => true,
        '#weight' => 1,
        '#description' => t('<p>Recommended values:</p><ul><li>.125 = 3 Hours</li><li>.25 = 6 Hours</li><li>.5 = 12 Hours</li><li>1 = Daily</li><li>7 = Weekly</li></ul>'),
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
        '#weight' => 3
    );

    return $form;
}

/**
 * Form submission
 *
 * Saves the API key to the database (or wherever)
 */
function panopticon_connector_admin_form_submit($form, &$form_state) {
    if (isset($form_state['values']['panopticon_connector_vcode'])) {
        $key = $form_state['values']['panopticon_connector_vcode'];
        variable_set('panopticon_connector_vcode', $key);
        variable_set('update_check_frequency', $form_state['values']['update_interval']);
        drupal_set_message(t('The configuration options have been saved.'));
    }
}
